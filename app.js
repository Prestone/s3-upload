const fs = require('fs');
const AWS = require('aws-sdk');
const dot = require('dotenv').config({path: __dirname + '/.env'});



const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});


console.log(s3);

const fileName = 'testfile.csv';


const uploadFile = () => {
  fs.readFile(fileName, (err, data) => {
     if (err) throw err;
     const params = {
         Bucket: 'image-upload-check', // my bucket name
         Key: 'testfile.csv', // my file name
         Body: JSON.stringify(data, null, 2)
     };
     s3.upload(params, function(s3Err, data) {
         if (s3Err) throw s3Err
         console.log(`File uploaded successfully at ${data.Location}`)
     });
  });
};

uploadFile();